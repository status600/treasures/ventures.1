


the_name="poetry_uv"


#
#	source /habitat/_controls/source_1.sh
#
git config --global --add safe.directory /habitat

#----
#
#	install
#
cd /habitat && rm -rf .venv
pip install uv
cd /habitat && rm requirements.txt
cd /habitat && uv pip compile pyproject.toml -o requirements.txt

sleep 1

cd /habitat && uv venv
cd /habitat && . /habitat/.venv/bin/activate
cd /habitat && uv pip sync requirements.txt

apt install unzip; curl -fsSL https://bun.sh/install | bash; . /root/.bashrc
#
#----


# deactivate


export PATH=$PATH:/habitat/venues/stages/poetry_uv/__dictionary


