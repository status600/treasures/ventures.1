

import os
import subprocess

def start_detached_process(command):
    # Fork the parent process
    pid = os.fork()
    if pid == 0:
        # This is the child process
        # Detach the child process from the parent process
        os.setsid()
        # Execute the command in the child process
        subprocess.Popen (command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        # Exit the child process
        os._exit(0)
    else:
        # This is the parent process
        # Return the PID of the child process
        return pid

# Example usage:
if __name__ == '__main__':
    command_to_execute = "python3 -m http.server 8081 &"
    detached_process_pid = start_detached_process(command_to_execute)
    print(f"Detached process started with PID: {detached_process_pid}")
