
'''
	from vaccines.adventures.sanique.adventure import sanique_adventure
	sanique_adventure ({
		"harbor_port": "10000",
		"inspector_port": "10001"
	})
'''

from sanique._controls.on import turn_on_sanique
from sanique._controls.off import turn_off_sanique
from sanique._controls.is_on import check_sanique_status
from sanique._controls.refresh import refresh_sanique

def sanique_adventure (packet):
	inspector_port = packet ["inspector_port"]
	harbor_port = packet ["harbor_port"]
	
	adventure_name = packet ["adventure_name"]
	
	return {
		"name": adventure_name,
		"kind": "task",
		"turn on": {
			"adventure": turn_on_sanique ({
				"ports": {
					"harbor": harbor_port,
					"inspector": inspector_port
				}
			}),
		},
		"turn off": turn_off_sanique ({
			"ports": {
				"inspector": inspector_port
			}
		}),
		"is on": check_sanique_status ({
			"ports": {
				"inspector": inspector_port
			}
		}),
		"refresh": refresh_sanique ({
			"ports": {
				"inspector": inspector_port
			}
		})
	}