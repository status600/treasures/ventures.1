



'''
	python3 clique.py ventures on
'''

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../stages'
])

from ventures_map_1 import ventures
from ventures.clique import ventures_clique

import click	
def clique ():
	@click.group ()
	def group ():
		pass

	group.add_command (ventures_clique ({
		"ventures": ventures
	}))
	group ()
	
clique ()