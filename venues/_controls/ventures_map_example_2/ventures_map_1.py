
#++++
#
import os
import pathlib
from os.path import dirname, join, normpath
import sys
#
#
from sanique.adventure import sanique_adventure
#
#++++

this_folder = pathlib.Path (__file__).parent.resolve ()
the_map = str (
	normpath (join (
		os.getcwd (), 
		"ventures_map.JSON"
	))
)

http_adventure = {
	"name": "1",
	"kind": "process_identity",
	"turn on": {
		"adventure": [ 
			"python3",
			"-m",
			"http.server",
			"8080"
		],
	}
}

print ("ventures_map_1")

from ventures import ventures_map
ventures = ventures_map ({
	"map": the_map,
	"ventures": [
		http_adventure,
		
		sanique_adventure ({
			"adventure_name": "adventure_1_sanique",
			
			"harbor_port": "10002",
			"inspector_port": "10003"
		}),
		sanique_adventure ({
			"adventure_name": "adventure_2_sanique",
			
			"harbor_port": "10000",
			"inspector_port": "10001"
		})
	]
})

